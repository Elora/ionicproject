import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {BackendProvider} from "../../providers/backend/backend";
import {ProductModel} from "../../models/Product";
// import { PipesModule } from "../../pipes/pipes.module";


/**
 * Generated class for the TestPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-test',
  templateUrl: 'test.html',
})
export class TestPage {

  name: string
  // products = new ProductModel();
products:ProductModel [];
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    // this.name= this.navParams.get('name');
    this.products= this.navParams.get('products');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TestPage');
  }



}
