import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { TestPage } from "../test/test";
import { BackendProvider} from "../../providers/backend/backend";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController, public alertCtrl: AlertController, public backendP: BackendProvider) {

  }
  afficher() :void {
    this.backendP.getProducts().subscribe( products => {
      this.navCtrl.push(TestPage, {products: products});
    });

  }

  presentPrompt() {
    const prompt = this.alertCtrl.create({
      title: 'Qui êtes vous ?',
      inputs: [
        {
          placeholder:"name",
          name:"name",
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Valider',
          handler: data => {
            if(data.name!=null){
            this.navCtrl.push(TestPage, {
              name: data.name
            })}else{
              return false;
            }
          }

        }
      ]
    });
    prompt.present();
  }

}
