import {Component, ViewChild} from '@angular/core';
import {Nav, Platform} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {AlertController} from "ionic-angular";
import { HomePage } from '../pages/home/home';
import {TestPage} from "../pages/test/test";


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage:any = HomePage;
  pages: Array<{title: string, component: any, alert: boolean}>;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, public alertCtrl: AlertController) {
    this.pages = [
      { title: 'Home', component: HomePage, alert: false },
      { title: 'Test', component: TestPage, alert: true }
    ];
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });

  }
  openPage(page) {

    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
   if(page.alert) {
     // this.homeCtrl.presentPrompt();
      const prompt = this.alertCtrl.create({
        title: 'Qui êtes vous ?',
        inputs: [
          {
            placeholder:"name",
            name:"name",
          },
        ],
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            handler: data => {
              console.log('Cancel clicked');
            }
          },
          {
            text: 'Valider',
            handler: data => {
              if(data.name!=null){
                this.nav.push(TestPage, {
                  name: data.name
                })}else{

                return false;
              }
            }

          }
        ]
      });
      prompt.present();
    }else{
      this.nav.setRoot(page.component);
    }
    // this.nav.setRoot(page.component);
  }
}

