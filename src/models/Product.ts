import {DateTime} from "ionic-angular";

export class ProductModel {
  id: number;
  name: string;
  categorie_id: number;
  menu_id: number;
  created_at: DateTime;
  updated_at: DateTime;
  categorie: {};
  menu: {};

}
