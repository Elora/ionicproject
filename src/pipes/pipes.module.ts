import { NgModule } from '@angular/core';
import { FirstLetterUpperCasePipe } from './first-letter-upper-case/first-letter-upper-case';
import { AllLetterUpperCasePipe } from './all-letter-upper-case/all-letter-upper-case';
@NgModule({
	declarations: [
    FirstLetterUpperCasePipe,
    AllLetterUpperCasePipe],
	imports: [],
	exports: [
    FirstLetterUpperCasePipe,
    AllLetterUpperCasePipe]
})
export class PipesModule {}
