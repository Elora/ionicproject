import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the BackendProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class BackendProvider {
  private url: string;
  private header = new Headers();
  public endpoints = {
    products : "/products",
    menus : "/menus",
    categories : "/categories"
  }

  constructor(public http: Http) {
    console.log('Hello BackendProvider Provider');
    this.url ="https://cors-anywhere.herokuapp.com/http://fast-badlands-48562.herokuapp.com/api/1.0";
  }

  getProducts(){
    return this.http.get((this.url+this.endpoints.products), {headers: this.header}).map(response => response.json());
  }

  getProduct(id:number){
    return this.http.get((this.url+this.endpoints.products+id), {headers: this.header}).map(response => response.json());
  }

}
